﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using Xunit;
using FluentAssertions;
using AutoFixture;
using AutoFixture.Xunit2;
using Booking.Domain;

namespace Booking.Domain.UnitTests
{
    public class BookingTests
    {
        protected  Fixture Fixture { get;  }
        public BookingTests()
        {
            Fixture =new Fixture();
        }
        public class AddGuestToBooking : BookingTests
        {

            Booking NewBooking(Country countryCode, string roomType, string title, string firstName,string lastName)
            {
                return  Booking.Factory.NewBooking(new Hotel()
                    {
                        Name = Fixture.Create<string>(),
                        CountryCode = countryCode,
                    },
                    roomType,
                    Guest.Factory.CreateGuest(firstName, lastName, title));
            }
            
            [Fact]
            public void When_creating_a_booking_the_guest_is_added()
            {
                var sut = NewBooking(Country.DK, "DOUBLE", Fixture.Create<string>(),
                    Fixture.Create<string>(), Fixture.Create<string>());
                sut.Guests.Count().Should().Be(1);
            }

            [Theory]
            [InlineData("DOUBLE")]
            [InlineData("TWIN")]
            [InlineData("TRIPLE")]
            public void When_adding_a_guest_to_a_booking_the_guest_is_added(string roomType)
            {
                var sut = NewBooking(Country.DK, roomType, Fixture.Create<string>(),
                    Fixture.Create<string>(), Fixture.Create<string>());

                var addedGuest = Guest.Factory.CreateGuest("Test", "Testsson");
                sut.AddGuest(addedGuest);

                sut.Guests.Should().Contain(addedGuest);
            }

            [Theory]
            [InlineData("SINGLE", 1)]
            [InlineData("DOUBLE", 2)]
            [InlineData("TWIN", 3)]
            [InlineData("TRIPLE", 3)]
            public void When_room_is_full_a_guest_cannot_be_added(string roomType, int capacity)
            {
                var sut = NewBooking(Country.DK, roomType, Fixture.Create<string>(),
                    Fixture.Create<string>(), Fixture.Create<string>());

                Guest addedGuest;
                for (var i = 1; i < capacity; i++ )
                {
                    addedGuest = Guest.Factory.CreateGuest(Fixture.Create<string>(), Fixture.Create<string>());
                    sut.AddGuest(addedGuest);
                }

                addedGuest = Guest.Factory.CreateGuest(Fixture.Create<string>(), Fixture.Create<string>());

                sut.AddGuest(addedGuest).Should().BeFalse("the room is full");
            }
        }
    }

   
}
