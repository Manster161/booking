To whom who is reading this.

Following the instructions and implementing this in 2-4 leaves much undone.

The idea:
Parameters to the REST service are defined in the contract solution.
The controller has a bookingService injected to handle the input.
The controller would like sanity checks on the input.
Return value of the controller should be an HTTP response and not the void. In this case for the PUT command (the booking is known) a success would be a 202 for a success.
The command service should be called by posting a message on some Bus (not implemented, handler pattern)
The room type identified by a string sounds sketchy. I would have preferred an enum.
Room types should be set per hotel by some configuration. I doubt there are standardized room types and any hotel should have the possibility to define its own.
It's hard to find a good place to the Title validation. For now it put it in a domain service since the logic does not fit into the the domain object itself.
A successful booking raises a domain event that could be picked up by an Eventstore or lets say an Email confirmation or any other system interested in the addGuest event.
The fetchboking (not implemented) should be implemented in the Query part of the solution giving a view of the domain data.

Tests: I have only added the minimum amount of tests to verify the assignment. Otherwise a coverage of less then 100 would not be acceptable.
