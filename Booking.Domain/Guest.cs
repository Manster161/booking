﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Booking.Domain
{
    public class Guest
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string Title { get; protected set; }

        protected Guest(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public void SetTitle(string title)
        {
            Title = title;
        }


        public static class Factory
        {
            public static Guest CreateGuest(string firstName, string lastName)
            {
                return CreateGuest(firstName, lastName, string.Empty);
            }

            public static Guest CreateGuest(string firstName, string lastName, string title)
            {
                return new Guest(firstName, lastName);
            }
        }
    }
}
