﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Domain.Interfaces
{
    public interface IBookingRepository :  IRepository<Booking, Guid>
    {
    }
}
