﻿namespace Booking.Domain.Interfaces
{
    public interface IRepository<T, TId>
    {
        T Get(TId id);
        void Save(T entity);
    }
}