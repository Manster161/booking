﻿using System;
using System.Collections.Generic;
using System.Linq;
using Booking.Domain.Extensions;

namespace Booking.Domain
{
    public class Booking : Aggregate
    {
        private readonly List<Guest> _guests = new List<Guest>();

        protected Booking(Hotel hotel, string roomType, Guest guest)
        {
            Hotel = hotel;
            RoomType = roomType;
            _guests.Add(guest);
            Id = Guid.NewGuid();
        }

        public IEnumerable<Guest> Guests => _guests;
        public string RoomType { get; }
        public Hotel Hotel { get; }

        public bool AddGuest(Guest guest)
        {
            if (GuestAllreadyPresent(guest) || IsFull()) return false;

            _guests.Add(guest);
            return true;

        }

        private bool IsFull()
        {
            return _guests.Count() >= Hotel.BedsInRoom(RoomType);
        }

        private bool GuestAllreadyPresent(Guest guest)
        {
            if (_guests.Any())
            {
                _guests.Select(g => g.SameGuest(guest)).Aggregate((res, item) => res && item);
            }

            return false;
        }

        public static class Factory
        {
            public static Booking NewBooking(Hotel hotel, string roomType, Guest guest)
            {
                return new Booking(hotel, roomType, guest);
            }
        }
    }

    public abstract class Aggregate
    {
        public Guid Id { get; protected set; }
    }
}