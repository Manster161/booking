﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Domain
{
    public class Hotel
    {
        public string Name { get; set; }
        public Country CountryCode { get; set; }
        private readonly IDictionary<string, int> _roomSizes = new Dictionary<string, int>();

        public IEnumerable<string> RoomTypes { get; } = new[] {"SINGLE", "DOUBLE", "TWIN", "TRIPLE"};

        public Hotel()
        {
            //Should be in some kind of configuration
            _roomSizes.Add("SINGLE", 1);
            _roomSizes.Add("DOUBLE", 2);
            _roomSizes.Add("TWIN", 2);
            _roomSizes.Add("TRIPLE", 3);
        }
        public int BedsInRoom(string roomType)
        {
            if (_roomSizes.ContainsKey(roomType.ToUpper()))
            {
                return _roomSizes[roomType];
            }
            else
            {
                throw new ArgumentException($"Unknown room type {roomType}");
            }
        }
    }
}
