﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Domain.Extensions
{
    public static class GuestExtensions
    {
        public static bool SameGuest(this Guest me, Guest them)
        {
            if (them == null)
                return false;

            return (me.FirstName.Equals(them.FirstName, StringComparison.InvariantCultureIgnoreCase) &&
                    me.LastName.Equals(them.LastName, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
