﻿using System;
using Booking.Domain;

namespace Booking.Command.Commands
{
    /// <inheritdoc />
    public class AddGuestToBooking : ICommand
    {
        protected AddGuestToBooking(Guid bookingId, Guest guest)
        {
            BookingId = bookingId;
            Guest = guest;
        }

        public Guid BookingId { get; }
        public Guest Guest { get; }

        public static AddGuestToBooking Create(Guid bookingId, Guest guest)
        {
            return new AddGuestToBooking(bookingId, guest);
        }
    }
}