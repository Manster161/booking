﻿using System;

namespace Booking.Command
{
    public class CommandResponse
    {
        protected CommandResponse(Guid? aggregateId, bool success)
        {
            AggregateId = aggregateId;
            Success = success;
        }

        public Guid? AggregateId { get; }
        public bool Success { get; }

        public static class Factory
        {
            public static CommandResponse Ok(Guid aggregateId)
            {
                return new CommandResponse(aggregateId, true);
            }

            public static CommandResponse Fail(Guid? aggregateId = null)
            {
                return new CommandResponse(aggregateId, false);
            }
        }
    }
}