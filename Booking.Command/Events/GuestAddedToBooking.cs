﻿using System;
using Booking.Domain;
using Booking.Infrastructure;
using Newtonsoft.Json;

namespace Booking.Command.Events
{
    public class GuestAddedToBooking : IEvent
    {
        protected GuestAddedToBooking(Guid id, string payload)
        {
            AggregateId = id;
            Payload = payload;
        }

        public string Payload { get; }
        public Guid AggregateId { get; }

        public static class Factory
        {
            public static GuestAddedToBooking CreateEvent(Guid aggregateId, Guest guest)
            {
                return new GuestAddedToBooking(aggregateId, JsonConvert.SerializeObject(guest));
            }
        }
    }
}