﻿using Booking.Domain;

namespace Booking.Command.Services
{
    public interface IBookingDomainService
    {
        bool ValidateBooking(Domain.Booking booking, Guest guest);
    }
}