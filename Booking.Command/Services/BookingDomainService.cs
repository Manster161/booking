﻿using System;
using System.Linq;
using Booking.Domain;

namespace Booking.Command.Services
{
    /// <inheritdoc />
    public class BookingDomainService : IBookingDomainService
    {
        private static readonly string[] _germanTitles = {"Herr", "Frau", "Dr"};

        public bool ValidateBooking(Domain.Booking booking, Guest guest)
        {
            var validationSuccess = true;
            if (booking.Hotel.CountryCode == Country.DE) validationSuccess = HasGermanTitle(guest);

            return validationSuccess;
        }

        private bool HasGermanTitle(Guest guest)
        {
            return _germanTitles.Contains(guest.Title, StringComparer.CurrentCultureIgnoreCase);
        }
    }
}