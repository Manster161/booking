﻿using Booking.Command.Commands;

namespace Booking.Command.Services
{
    public interface IBookingCommandService
    {
        CommandResponse When(AddGuestToBooking command);
    }
}