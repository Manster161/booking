﻿using Booking.Command.Commands;
using Booking.Command.Events;
using Booking.Domain.Interfaces;
using Booking.Infrastructure;

namespace Booking.Command.Services
{
    public class BookingCommandService : IBookingCommandService
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IBookingDomainService _bookingService;
        private readonly IBus _bus;

        public BookingCommandService(IBus bus, IBookingDomainService bookingService,
            IBookingRepository bookingRepository)
        {
            _bus = bus;
            _bookingService = bookingService;
            _bookingRepository = bookingRepository;
        }

        public CommandResponse When(AddGuestToBooking command)
        {
            var booking = _bookingRepository.Get(command.BookingId);

            if (_bookingService.ValidateBooking(booking, command.Guest) && booking.AddGuest(command.Guest))
            {
                _bookingRepository.Save(booking);
                _bus.RaiseEvent(GuestAddedToBooking.Factory.CreateEvent(command.BookingId, command.Guest));
                return CommandResponse.Factory.Ok(booking.Id);
            }

            return CommandResponse.Factory.Fail(booking.Id);
        }
    }
}