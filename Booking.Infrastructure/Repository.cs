﻿using Booking.Domain.Interfaces;

namespace Booking.Infrastructure
{
    public  abstract class Repository<T, TId> : IRepository<T, TId>
    {
        public  abstract  T Get(TId id);
        public abstract void Save(T entity);
        
    }
}