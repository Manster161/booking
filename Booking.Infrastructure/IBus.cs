﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Text;

namespace Booking.Infrastructure
{
    public interface IBus
    {
        void RaiseEvent(IEvent @event);
    }

    public interface IEvent
    {
        Guid AggregateId { get; }
    }
}
