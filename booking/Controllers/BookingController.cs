﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using booking.Services;
using Booking.Contract;
using Microsoft.AspNetCore.Mvc;

namespace booking.Controllers
{
    public class BookingController
    {
        private readonly IBookingService _bookingService;

        public BookingController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] Guest value)
        {
            _bookingService.AddGuestToBooking(id, value);
        }
    }
}
