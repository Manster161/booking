﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using booking.Extensions;
using Booking.Command;
using Booking.Command.Services;
using Booking.Contract;

namespace booking.Services
{
    public class BookingService : IBookingService
    {
        private readonly IBookingCommandService _bookingCommandService;

        public BookingService(IBookingCommandService bookingCommandService)
        {
            _bookingCommandService = bookingCommandService;
        }
        public void AddGuestToBooking(Guid booingId, Guest guest)
        {
            _bookingCommandService.When(Booking.Command.Commands.AddGuestToBooking.Create(booingId, guest.ToDomain()));
        }
    }
}
