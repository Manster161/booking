﻿using System;
using Booking.Contract;

namespace booking.Services
{
    public interface IBookingService
    {
        void AddGuestToBooking(Guid booingId, Guest guest);
    }
}