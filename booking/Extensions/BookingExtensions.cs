﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booking.Contract;

namespace booking.Extensions
{
    public static class BookingExtensions
    {
        public static Booking.Domain.Guest ToDomain(this Guest guest)
        {
            return Booking.Domain.Guest.Factory.CreateGuest(guest.FirstName, guest.LastName, guest.Title);
        }
    }
}
