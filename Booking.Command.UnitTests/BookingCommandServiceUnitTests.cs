﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.Kernel;
using AutoFixture.Xunit2;
using Booking.Command.Commands;
using Booking.Command.Services;
using Booking.Domain;
using Booking.Domain.Interfaces;
using Booking.Infrastructure;
using Moq;
using Xunit;
using  FluentAssertions;
using Booking = Booking.Domain.Booking;

namespace Booking.Command.UnitTests
{

   
    public class BookingCommandServiceUnitTests
    {

       

        protected BookingCommandService Sut;
        protected Mock<IBus> Bus;
        protected Mock<IBookingRepository> BookingRepository;
        protected BookingDomainService BookinDomainService;
        public Fixture Fixture;
        public BookingCommandServiceUnitTests()
        {
            Bus = new Mock<IBus>();
            BookingRepository = new Mock<IBookingRepository>();
            BookinDomainService = new BookingDomainService();
            Sut = new BookingCommandService(Bus.Object, BookinDomainService, BookingRepository.Object);
            Fixture = new Fixture();
            Fixture.Register<Guest>(() => Guest.Factory.CreateGuest(Fixture.Create<string>(), Fixture.Create<string>()));

        }


        public class  When : BookingCommandServiceUnitTests
        {


            [Theory]
            [AutoData]
            public void When_adding_a_guest_to_a_hotel_in_germany_a_title_is_needed(Hotel hotel, string fristName, string lastName, string firstName2, string lastName2)
            {
                var bookingGuid = Guid.NewGuid();

               hotel.CountryCode = Country.DE;


                var additionalGuest = Guest.Factory.CreateGuest(firstName2, lastName2);
                additionalGuest.SetTitle("NoGermanTitle");

                var b = CreateBooking(hotel,Guest.Factory.CreateGuest(fristName, lastName),2);
                BookingRepository.Setup(repo => repo.Get(b.Id)).Returns(b);
                


                var commandResponse =  Sut.When(AddGuestToBooking.Create(b.Id, additionalGuest));

                commandResponse.Success.Should().BeFalse();
            }

            [Theory]
            [AutoData]
            public void When_adding_a_guest_with_a_germant_title_to_a_booking_in_germany_its_a_success(Hotel hotel, string fristName, string lastName, string firstName2, string lastName2)
            {
                hotel.CountryCode = Country.DE;
                var additionalGuest = Guest.Factory.CreateGuest(firstName2, lastName2);
                additionalGuest.SetTitle("NoGermanTitle");
                

                var b = CreateBooking(hotel, Guest.Factory.CreateGuest(fristName, lastName), 2);

                BookingRepository.Setup(repo => repo.Get(b.Id)).Returns(b);

                var commandResponse = Sut.When(AddGuestToBooking.Create(b.Id, additionalGuest));

                commandResponse.Success.Should().BeFalse();
            }

            private Domain.Booking CreateBooking(Hotel hotel, Guest g, int minRoomSize)
            {
                var random = new Random(DateTime.Now.Millisecond);

                var rooms = hotel.RoomTypes.Where(r => hotel.BedsInRoom(r) >= minRoomSize);
               
                return Domain.Booking.Factory.NewBooking(
                    hotel,
                    rooms.ElementAt(random.Next(rooms.Count() -1 )),
                    g);
            }
        }
    }
}
